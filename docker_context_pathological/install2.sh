apt-get update
apt-get install -y virtualenv cmake git python3.6-tk python3.6-dev python3-pip openslide-tools tesseract-ocr


# virtual_env
virtualenv -p python3.6 pathological_env
source pathological_env/bin/activate


pip install matplotlib==3.1.1
pip install scikit-image opencv-python pydot pandas watchdog tqdm openslide-python scikit-plot pytesseract
pip install h5py==2.10.0
pip install tensorflow==1.14 keras==2.2.5

