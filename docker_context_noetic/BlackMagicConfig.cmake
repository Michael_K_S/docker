SET(BlackMagic_VERSION 11.5.1)
SET(BlackMagic_VERSION_MAJOR  11)
SET(BlackMagic_VERSION_MINOR  5)
SET(BlackMagic_VERSION_PATCH  1)
SET(BlackMagic_VERSION_TWEAK  0)

set(BlackMagic_INCLUDE_DIRS "/usr/include/blackmagic")

set(BlackMagic_LIBS "/usr/lib/libDeckLinkAPI.so")
set(BlackMagic_LIBRARIES ${BlackMagic_LIBS})

