SET(Tesseract_VERSION 4.1.1)
SET(Tesseract_VERSION_MAJOR  4)
SET(Tesseract_VERSION_MINOR  1)
SET(Tesseract_VERSION_PATCH  1)
SET(Tesseract_VERSION_TWEAK  2)

set(TESSDATA_PREFIX "/usr/share/tesseract-ocr/4.00/tessdata/")    
set(Tesseract_INCLUDE_DIRS "/usr/include/tesseract")

set(Tesseract_LIBS "/usr/lib/x86_64-linux-gnu/libtesseract.so")
set(Tesseract_LIBRARIES ${Tesseract_LIBS})
